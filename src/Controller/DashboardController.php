<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;


use App\Entity\Admin\AppModule;
use App\Entity\Admin\Setting;
use App\Entity\Admin\Terminal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * @Route("/")
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class DashboardController extends AbstractController
{
    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="app_dashboard")
     */
    public function index(): Response
    {
        /* @var $terminal Terminal */
        $terminal =  $this->getUser()->getTerminal();
        if(empty($terminal)){
            return $this->redirect($this->generateUrl('homepage'));
        }
        return $this->redirect($this->generateUrl('bank_index'));
    }

}
