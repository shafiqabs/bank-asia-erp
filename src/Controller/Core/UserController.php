<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;


use App\Entity\Admin\Location;
use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Profile;
use App\Entity\Core\UserReporting;
use App\Entity\User;
use App\Form\Core\EditProfileFormType;
use App\Form\Core\ProfileFormType;
use App\Form\Core\RegistrationFormType;
use App\Form\Type\ChangePasswordType;
use App\Repository\Core\UserReportingRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\SecurityBillingBundle\Repository\DeploymentPostRepository;

/**
 * Controller used to manage current user.
 *
 * @Route("/core/user")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UserController extends AbstractController
{

    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="core_user")
     * @Route("/", methods={"GET"}, name="core_user_index")
     */
    public function index(): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entities = $this->getDoctrine()->getRepository(User::class)->findBy(array('terminal'=>$terminal));
        return $this->render('core/user/index.html.twig',['entities'=> $entities]);
    }

    /**
     * Lists all Post entities.
     * @Route("/reporting", methods={"GET"}, name="user_reporting")
     */
    public function userReporting(): Response
    {
        return $this->render('core/user/userReporting.html.twig');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="user_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function status(User $user): Response
    {

        $status = $_REQUEST['status'];
        if($status == "false"){
            $user->setEnabled(false);
        }else{
            $user->setEnabled(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     * @Route("/register", methods={"GET", "POST"}, name="core_user_register")
     */
    public function register(Request $request , UserPasswordEncoderInterface $passwordEncoder,TranslatorInterface $translator, UserReportingRepository $reportingRepository): Response
    {
        $profile = new Profile();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(ProfileFormType::class, $profile, array('terminal' => $terminal,'userRepo'=> $userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $password =  $data['profile_form']['user']['password']['first'];
            $profile->getUser()->setPassword(
                $passwordEncoder->encodePassword(
                    $profile->getUser(),
                    $password
                )
            );
            $profile->getUser()->setEmail($profile->getUser()->getUsername());
            $profile->getUser()->setTerminal($terminal);
            $em->persist($profile);
            $em->flush();
            $reportingRepository->insertUser($profile->getUser());
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('core_user');
        }
        return $this->render('core/user/register.html.twig', [
            'id' => 'postForm',
            'post' => $profile,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="core_user_edit")
     */
    public function edit(Request $request, TranslatorInterface $translator ,$id): Response
    {
        $data = $request->request->all();
        $post = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $id]);
        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(EditProfileFormType::class, $post->getProfile(), array('terminal' => $terminal,'userRepo' => $userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('core_user_edit',array('id'=> $post->getId()));
        }
        return $this->render('core/user/editRegister.html.twig', [
            'entity' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Check if username available for registering
     * @Route("/register/username/available", methods={"GET"}, name="core_user_username_available")
     * @param   string
     * @return  bool
     */
    function isUsernameAvailable(Request $request) : Response
    {
        $data = $request->query->all();
        $username = $data['registration_form']['username'];
        $post = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username'=> $username]);
        if (empty($post)){
            $process = "true";
        }else{
            $process =  "false";
        }
        return new Response($process);

    }

    /**
     * Check if username available for registering
     * @Route("editable/username/available", methods={"GET"}, name="core_user_editable_username_available")
     * @param   string
     * @return  bool
     */
    function isEditableUsernameAvailable(Request $request) : Response
    {
        $data = $request->query->all();
        $username = $data['registration_form']['username'];
        $post = $this->getDoctrine()->getRepository(User::class)->findBy(['username'=> $username]);
        if (count($post) > 1 ){
            $process = "true";
        }else{
            $process =  "false";
        }
        return new Response($process);

    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/reset-password", methods={"GET", "POST"}, name="user_change_password")
     */
    public function changeUserPassword(Request $request,UserRepository $userRepository , TranslatorInterface $translator ,UserPasswordEncoderInterface $passwordEncoder,$id): Response
    {

        $user = $this->getUser();

        /* @var $entity User */

        $entity = $userRepository->findOneBy(['terminal'=> $user->getTerminal(),'id'=>$id]);
        if($entity){
            $password = $user->getTerminal()->getCoreDomain()->getResetPassword();
            $entity->setPassword(
                $passwordEncoder->encodePassword(
                    $entity,
                    $password
                )
            );
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
        }
        return $this->redirectToRoute('core_user');

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="core_user_show", options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $domain = $this->getUser()->getTerminal();
        $entity = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $id,'terminal'=>$domain]);
        return $this->render('core/user/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="core_user_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="core_user_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataTable(Request $request,UserRepository $userRepository)
    {

        $query = $_REQUEST;
        $terminal = $this->getUser()->getTerminal();
        $iTotalRecords = $userRepository->count(array('terminal'=> $terminal));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $userRepository->findBySearchQuery($terminal->getId(),$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post User */

        foreach ($result as $post):

            $enabled = $post['enabled'] == 1 ? "Enabled" : "Disabled";
            $editUrl = $this->generateUrl('core_user_edit',array('id' => $post['id']));
            $passwordUrl = $this->generateUrl('user_change_password',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('core_user_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('core_user_show',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $userGroup          = $post['userGroup'],
                $name               = $post['name'],
                $username           = $post['username'],
                $designation        = $post['designation'],
                $mobile             = $post['mobile'],
                $email              = $post['email'],
                $department         = $post['department'],
                $branch             = $post['branch'],
                $status             = $enabled,
                $action             ="<div class='dropdown'>
<button type='button' class='btn btn-notify dropbtn'><i class='fas fa-ellipsis-h'></i></button>
<div class='dropdown-content'>
<a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a>
<a href='{$editUrl}'>Edit</a>
<a href='{$passwordUrl}'>Reset Password</a>
<a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a>
</div></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/reporting-data-table", methods={"GET", "POST"}, name="core_reporting_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataReportingTable(Request $request,UserRepository $userRepository)
    {

        $query = $_REQUEST;
        $terminal = $this->getUser()->getTerminal();
        $iTotalRecords = $userRepository->count(array('terminal'=> $terminal));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $userRepository->findUserReporting($terminal->getId(),$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post User */

        foreach ($result as $post):


            $reportingId = empty($post['reportingId']) ? 0 : $post['reportingId'];
            $reportingName = empty($post['reportingName']) ? "Empty" : $post['reportingName'];
            $relieverId = empty($post['relieverId']) ? 0 : $post['relieverId'];
            $relieverName = empty($post['relieverName']) ? "Empty" : $post['relieverName'];

            $enabled = $post['enabled'] == 1 ? "Enabled" : "Disabled";

            $viewUrl = $this->generateUrl('core_user_show',array('id'=> $post['id']));
            $userDataTable = $this->generateUrl('core_reporting_user_data_table');
            $reportTo ="<a  data-type='select' class='editable select2' id='reportTo' data-name='reportTo' href='javascript:' data-source='{$userDataTable}'  data-url='{$this->generateUrl('core_reporting_user_inline_update')}' data-pk='{$post['id']}' data-value='{$reportingId}'>{$reportingName}</a>";

            $reliever ="<a  data-type='select' class='editable' id='relieverTo' data-name='relieverTo' href='javascript:' data-source='{$userDataTable}'  data-url='{$this->generateUrl('core_reporting_user_inline_update')}' data-pk='{$post['id']}' data-value='{$relieverId}'>{$relieverName}</a>";

            $records["data"][] = array(
                $id                 = $i,
                $name               = $post['name'],
                $branch             = '',
                $designation        = $post['designationName'],
                $operationalDesignation        = $post['designationName'],
                $department         = $post['departmentName'],
                $reportTo           = $reportTo,
                $reliever           = $reliever,
                $status             = $enabled,
                $action             ="<div class='dropdown'>
<button type='button' class='btn btn-notify dropbtn'><i class='fas fa-ellipsis-h'></i></button>
<div class='dropdown-content'>
<a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a>
</div></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/reporting-user-data-table", methods={"GET", "POST"}, name="core_reporting_user_data_table", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function userDataTable(Request $request, UserRepository $userRepository)
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $entities = $userRepository->findUserAssignReporting($terminal);
        $items = array();
        $items[] = array('value' => '','text'=> '-Assign Reporting Manager-');
        foreach ($entities as $entity):
            $items[] = array('value' => $entity['id'],'text'=> $entity['name']);
        endforeach;
        $items[]=array('value' => '0','text'=> 'No Reporting');
        return new JsonResponse($items);

    }

    /**
     * @Route("/reporting-user-inline-update", methods={"GET", "POST"}, name="core_reporting_user_inline_update", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function inlineUpdate(Request $request, UserRepository $userRepository , UserReportingRepository $reportingRepository)
    {

        $data = $request->request->all();
        $entity = $reportingRepository->findOneBy(array('user'=> $data['pk']));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        if($data['name'] == 'reportTo'){
            $setValue = $userRepository->find($data['value']);
            if($setValue){
                $entity->setReportTo($setValue);
            }else {
                $entity->setReportTo(NULL);
            }
        }elseif($data['name'] == 'relieverTo'){
            $setValue = $userRepository->find($data['value']);
            if($setValue){
                $entity->setRelieverTo($setValue);
            }else {
                $entity->setRelieverTo(NULL);
            }
        }else{
            $setName = 'set'.$data['name'];
            $setValue = $data['value'];
            $entity->$setName($setValue);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

}
