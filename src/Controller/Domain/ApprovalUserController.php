<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;
use App\Entity\Domain\ApprovalUser;
use App\Form\Domain\ApprovalUserFormType;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;




/**
 * @Route("/domain/approval-user")
 * @Security("is_granted('ROLE_DOMAIN_ADMIN') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ApprovalUserController extends AbstractController
{
    
    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="approval_user")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SBS_ADMIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function index(Request $request, ApprovalUserRepository $repository, TranslatorInterface $translator): Response
    {

        $terminal = $this->getUser()->getTerminal();
        $pagination = $repository->findBy(array('terminal'=>$terminal->getId()),array('ordering'=>'ASC'));
        $entity = new ApprovalUser();
        $data = $request->request->all();
        $form = $this->createForm(ApprovalUserFormType::class, $entity, array('terminal' => $terminal))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $exist = $repository->findOneBy(array('user'=>$entity->getUser()));
        $formValidation = new FormValidationManager();
        $errors = $formValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid() and empty($exist)) {
            $em = $this->getDoctrine()->getManager();
            $entity->setTerminal($terminal->getId());
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('approval_user');
        }
       // dd($errors);
        return $this->render('domain/approval-user/index.html.twig', [
            'pagination'    => $pagination,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="approval_user_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function edit(Request $request, $id,ApprovalUserRepository $repository, TranslatorInterface $translator): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $pagination = $repository->findBy(array('terminal'=>$terminal),array('ordering'=>'ASC'));

        /* @var $entity ApprovalUser */

        $entity = $repository->findOneBy(array('terminal' => $terminal,'id' => $id));
        $form = $this->createForm(ApprovalUserFormType::class, $entity, array('terminal' => $terminal))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('approval_user');
        }
        return $this->render('domain/approval-user/index.html.twig', [
            'pagination'    => $pagination,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="approval_user_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SBS_ADMIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function status($id, ApprovalUserRepository $repository): Response
    {

        /* @var $entity ApprovalUser */
        $entity = $repository->findOneBy(array('id' => $id));
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/mandatory", methods={"GET"}, name="approval_user_mandatory" , options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SBS_ADMIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function mandatory($id, ApprovalUserRepository $repository): Response
    {

        /* @var $entity ApprovalUser */

        $entity = $repository->findOneBy(array('id' => $id));
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setIsMandatory(false);
        }else{
            $entity->setIsMandatory(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/rejected", methods={"GET"}, name="approval_user_rejected" , options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SBS_ADMIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function rejected($id, ApprovalUserRepository $repository): Response
    {

        /* @var $entity ApprovalUser */

        $entity = $repository->findOneBy(array('id' => $id));
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setIsRejected(false);
        }else{
            $entity->setIsRejected(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="approval_user_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SBS_ADMIN') or is_granted('ROLE_DOMAIN_ADMIN')")
     */

    public function delete($id, ApprovalUserRepository $repository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = $repository->findOneBy(array('terminal' => $terminal,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/invoice-item-ordering", methods={"GET"}, name="approval_user_ordering" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function invoiceItemOrdering(ApprovalUserRepository $repository): Response
    {

        $data = $_REQUEST;
        $id = $data['columnId'];
        $sorting = $data['sorting'];
        $entity = $repository->find($id);
        $entity->setOrdering($sorting);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

}
