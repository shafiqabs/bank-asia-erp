<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;


use App\Entity\Domain\BundleRoleGroup;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use App\Form\Domain\BundleRoleGroupFormType;
use App\Form\Domain\ModuleProcessFormType;
use App\Repository\Domain\BundleRoleGroupRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/domain/bundle-role-group")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BundleRoleGroupController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="domain_bundlerole")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $entitys = $this->getDoctrine()->getRepository(BundleRoleGroup::class)->findBy(array('terminal'=>$terminal));
        return $this->render('domain/bundleRoleGroup/index.html.twig',['entities' => $entitys]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="domain_bundlerole_new")
     */
    public function new(Request $request, TranslatorInterface $translator): Response
    {

        $entity = new BundleRoleGroup();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(BundleRoleGroupFormType::class, $entity, array('terminal' => $terminal,'userRepo'=>$userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setTerminal($this->getUser()->getTerminal()->getId());
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('domain_bundlerole_new');
            }
            return $this->redirectToRoute('domain_bundlerole');
        }
        return $this->render('domain/bundleRoleGroup/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="domain_bundlerole_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, BundleRoleGroup $entity, TranslatorInterface $translator): Response
    {
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(BundleRoleGroupFormType::class, $entity, array('terminal' => $terminal,'userRepo'=>$userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message); $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('domain_bundlerole_edit', ['id' => $entity->getId()]);
            }
            return $this->redirectToRoute('domain_bundlerole');
        }
        return $this->render('domain/bundleRoleGroup/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a moduleProcess entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="domain_bundlerole_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function delete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(BundleRoleGroup::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $entity->setIsDelete(true);
        $em->persist($entity);
        $em->flush();
        return new Response('Success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="domain_bundlerole_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id, BundleRoleGroupRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


}
