<?php


namespace App\Security;


use App\Entity\User as AppUser;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }
        if ($user->isDelete() == 1 and $user->getEnabled() == 0 ) {
            throw new CustomUserMessageAccountStatusException('Your user account has been deleted');
        }else if ($user->getEnabled() == 0 and $user->isDelete() == 0) {
            throw new CustomUserMessageAccountStatusException('Your user account has been disabled');
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // user account is expired, the user may be notified
        if ($user->isDelete() == 1) {
            throw new AccountExpiredException('...');
        }
    }
}