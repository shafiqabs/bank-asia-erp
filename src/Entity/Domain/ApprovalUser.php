<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Domain;

use App\Entity\Admin\AppModule;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\ApprovalUserRepository")
 * @ORM\Table(name="domain_approval_user")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 * @UniqueEntity(fields={"user"}, message="Sorry,that user name already exists!")
 */
class ApprovalUser
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $terminal;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="App\Entity\User",inversedBy="approvalUser")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $user;

     /**
     * @var Setting
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Setting")
     * @ORM\OrderBy({"name" = "ASC"})
      **/
    private  $departments;

     /**
     * @var Branch
     * @ORM\ManyToMany(targetEntity="App\Entity\Domain\Branch")
     * @ORM\JoinTable(name="domain_approve_user_branch")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private  $branches;


    /**
     * @var AppModule
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\AppModule", inversedBy="approvalUser")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private  $appModules;



    /**
     * @var ApprovalFinancialMatrix
     * @ORM\ManyToOne(targetEntity="ApprovalFinancialMatrix")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private  $financialMatrix;


    /**
     * @var ApprovalFinancialMatrix
     * @ORM\ManyToOne(targetEntity="ApprovalFinancialMatrix")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private  $financialMatrixCapex;

    /**
     * @var ApprovalFinancialMatrix
     * @ORM\ManyToOne(targetEntity="ApprovalFinancialMatrix")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private  $financialMatrixOpex;

    /**
     * @var ApprovalFinancialMatrix
     * @ORM\ManyToOne(targetEntity="ApprovalFinancialMatrix")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private  $financialMatrixExpense;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process;


     /**
     * @var array
     * @ORM\Column(type="array",nullable=true)
     */
    private $requisitionMode;


    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isMandatory;

      /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isRejected;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $ordering = 0;

    public function __construct()
    {
        $this->appModules = new ArrayCollection();
        $this->branches = new ArrayCollection();
        $this->departments = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isMandatory()
    {
        return $this->isMandatory;
    }

    /**
     * @param bool $isMandatory
     */
    public function setIsMandatory(bool $isMandatory)
    {
        $this->isMandatory = $isMandatory;
    }

    /**
     * @return int
     */
    public function getOrdering(): int
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering(int $ordering)
    {
        $this->ordering = $ordering;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isRejected()
    {
        return $this->isRejected;
    }

    /**
     * @param bool $isRejected
     */
    public function setIsRejected($isRejected)
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return AppModule
     */
    public function getAppModules()
    {
        return $this->appModules;
    }

    /**
     * @param AppModule $appModules
     */
    public function setAppModules($appModules)
    {
        $this->appModules = $appModules;
    }

    /**
     * @return Setting
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @param Setting $departments
     */
    public function setDepartments($departments)
    {
        $this->departments = $departments;
    }




    /**
     * @return Branch
     */
    public function getBranches()
    {
        return $this->branches;
    }

    /**
     * @param Branch $branches
     */
    public function setBranches($branches): void
    {
        $this->branches = $branches;
    }

    /**
     * @return ApprovalFinancialMatrix
     */
    public function getFinancialMatrix()
    {
        return $this->financialMatrix;
    }

    /**
     * @param ApprovalFinancialMatrix $financialMatrix
     */
    public function setFinancialMatrix($financialMatrix)
    {
        $this->financialMatrix = $financialMatrix;
    }

    /**
     * @return array
     */
    public function getRequisitionMode()
    {
        return $this->requisitionMode;
    }

    /**
     * @param array $requisitionMode
     */
    public function setRequisitionMode($requisitionMode)
    {
        $this->requisitionMode = $requisitionMode;
    }

    /**
     * @return ApprovalFinancialMatrix
     */
    public function getFinancialMatrixCapex()
    {
        return $this->financialMatrixCapex;
    }

    /**
     * @param ApprovalFinancialMatrix $financialMatrixCapex
     */
    public function setFinancialMatrixCapex(ApprovalFinancialMatrix $financialMatrixCapex)
    {
        $this->financialMatrixCapex = $financialMatrixCapex;
    }

    /**
     * @return ApprovalFinancialMatrix
     */
    public function getFinancialMatrixOpex()
    {
        return $this->financialMatrixOpex;
    }

    /**
     * @param ApprovalFinancialMatrix $financialMatrixOpex
     */
    public function setFinancialMatrixOpex(ApprovalFinancialMatrix $financialMatrixOpex)
    {
        $this->financialMatrixOpex = $financialMatrixOpex;
    }

    /**
     * @return ApprovalFinancialMatrix
     */
    public function getFinancialMatrixExpense()
    {
        return $this->financialMatrixExpense;
    }

    /**
     * @param ApprovalFinancialMatrix $financialMatrixExpense
     */
    public function setFinancialMatrixExpense( $financialMatrixExpense)
    {
        $this->financialMatrixExpense = $financialMatrixExpense;
    }





}
