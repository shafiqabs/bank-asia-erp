<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Domain;
use App\Entity\Admin\Location;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Terminalbd\SecurityBillingBundle\Entity\ServiceEmployee;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\BranchRepository")
 * @ORM\Table(name="domain_branch")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Branch
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="terminal", type="integer", nullable=true)
     */
    private $terminal;

    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $parent;


    /**
     * @var Branch
     * @ORM\OneToMany(targetEntity="App\Entity\Domain\Branch", mappedBy="parent")
     */
    private $children;


    /**
     * @var Customer
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Customer", inversedBy="branches")
     **/
    protected $customer;

     /**
     * @var Location
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     */
    private $location;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $code;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $description;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $nameBn;

     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoiceAddressTo = "Manager";

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $branchType;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactPerson;

     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactMobile;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactPhone;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false, nullable=true)
     */
    private $slug;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $billingAddress;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $additionalPhone;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $postalCode;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $oldId;


     /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $oldClientId;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * @param string $nameBn
     */
    public function setNameBn($nameBn)
    {
        $this->nameBn = $nameBn;
    }

    public function getNameBanglaEnglish(){

        return $this->nameBn.' - '.$this->name;
    }

    /**
     * @param integer $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param string $contactPerson
     */
    public function setContactPerson(string $contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return string
     */
    public function getContactMobile()
    {
        return $this->contactMobile;
    }

    /**
     * @param string $contactMobile
     */
    public function setContactMobile(string $contactMobile)
    {
        $this->contactMobile = $contactMobile;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone(string $contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(string $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param string $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return string
     */
    public function getAdditionalPhone()
    {
        return $this->additionalPhone;
    }

    /**
     * @param string $additionalPhone
     */
    public function setAdditionalPhone(string $additionalPhone)
    {
        $this->additionalPhone = $additionalPhone;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode(string $postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getOldId()
    {
        return $this->oldId;
    }

    /**
     * @param int $oldId
     */
    public function setOldId(int $oldId)
    {
        $this->oldId = $oldId;
    }

    /**
     * @return int
     */
    public function getOldClientId()
    {
        return $this->oldClientId;
    }

    /**
     * @param int $oldClientId
     */
    public function setOldClientId(int $oldClientId)
    {
        $this->oldClientId = $oldClientId;
    }

    /**
     * @return Branch
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Branch $parent
     */
    public function setParent(Branch $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Branch
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getBranchType()
    {
        return $this->branchType;
    }

    /**
     * @param string $branchType
     */
    public function setBranchType(string $branchType)
    {
        $this->branchType = $branchType;
    }

    /**
     * @return string
     */
    public function getInvoiceAddressTo()
    {
        return $this->invoiceAddressTo;
    }

    /**
     * @param string $invoiceAddressTo
     */
    public function setInvoiceAddressTo($invoiceAddressTo)
    {
        $this->invoiceAddressTo = $invoiceAddressTo;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete)
    {
        $this->isDelete = $isDelete;
    }


}
