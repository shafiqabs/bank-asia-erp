<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Domain;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\ApprovalFinancialMatrixRepository")
 * @ORM\Table(name="dom_approval_matrix_process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ApprovalFinancialMatrix
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $terminal;


    /**
     * * @var AppModule
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\AppModule")
     **/
    private $module;

    /**
     * @var string
     * @ORM\Column(type="string", length=255  , nullable=true )
     */
    private $name;


     /**
     * @var float
     * @ORM\Column(type="float", nullable=true )
     */
    private $amount;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return AppModule
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param AppModule $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }


    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}
