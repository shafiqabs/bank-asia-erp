<?php

namespace App\Entity\Domain;

use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Profile;
use App\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Domain
 * @ORM\Table("domain")
 * @ORM\Entity(repositoryClass="App\Repository\Domain\DomainRepository")
 */
class Domain
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Admin\Terminal", inversedBy="coreDomain")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $terminal;


    /**
     * @var string
     * @ORM\Column(type="string", length=255  , nullable=true )
     */
    private $resetPassword;

     /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $navbar = "sidebar";

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $path;


    /**
     * @Assert\File(maxSize="1M")
     */
    protected $file;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $backgroundImagePath;


    /**
     * @Assert\File(maxSize="5M")
     */
    protected $backgroundImageFile;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $printHeaderPath;


    /**
     * @Assert\File(maxSize="2M")
     */
    protected $printHeaderFile;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $printFooterPath;


    /**
     * @Assert\File(maxSize="2M")
     */
    protected $printFooterFile;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Terminal
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param Terminal $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return string
     */
    public function getResetPassword()
    {
        return $this->resetPassword;
    }

    /**
     * @param string $resetPassword
     */
    public function setResetPassword($resetPassword)
    {
        $this->resetPassword = $resetPassword;
    }

    /**
     * Sets file.
     *
     * @param Domain $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Domain
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return "uploads/domain/{$this->getTerminal()->getId()}/";
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Sets backgroundImageFile.
     *
     * @param Domain $backgroundImageFile
     */
    public function setBackgroundImageFile(UploadedFile $backgroundImageFile = null)
    {
        $this->backgroundImageFile = $backgroundImageFile;
    }

    /**
     * Get backgroundImageFile.
     *
     * @return Domain
     */
    public function getBackgroundImageFile()
    {
        return $this->backgroundImageFile;
    }

    public function getAbsoluteBackgroundImagePath()
    {
        return null === $this->backgroundImagePath
            ? null
            : $this->getUploadRootDir().'/'.$this->backgroundImagePath;
    }

    public function backgroundImageUpload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getBackgroundImageFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getBackgroundImageFile()->getClientOriginalName();
        $this->getBackgroundImageFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->backgroundImagePath = $filename ;

        // clean up the file property as you won't need it anymore
        $this->backgroundImageFile = null;
    }


    /**
     * Sets printHeaderFile.
     *
     * @param Domain $printHeaderFile
     */
    public function setPrintHeaderFile(UploadedFile $printHeaderFile = null)
    {
        $this->printHeaderFile = $printHeaderFile;
    }

    /**
     * Get printHeaderFile.
     *
     * @return Domain
     */
    public function getPrintHeaderFile()
    {
        return $this->printHeaderFile;
    }

    public function getAbsolutePrintHeaderPath()
    {
        return null === $this->printHeaderPath
            ? null
            : $this->getUploadRootDir().'/'.$this->printHeaderPath;
    }

    public function getWebHeaderPath()
    {
        return null === $this->printHeaderPath
            ? null
            : $this->getUploadDir(). $this->printHeaderPath;
    }

    public function printHeaderUpload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getPrintHeaderFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getPrintHeaderFile()->getClientOriginalName();
        $this->getPrintHeaderFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->printHeaderPath = $filename ;

        // clean up the file property as you won't need it anymore
        $this->printHeaderFile = null;
    }

    /**
     * Sets printFooterFile.
     *
     * @param Domain $printFooterFile
     */
    public function setPrintFooterFile(UploadedFile $printFooterFile = null)
    {
        $this->printFooterFile = $printFooterFile;
    }

    /**
     * Get printFooterFile.
     *
     * @return Domain
     */
    public function getPrintFooterFile()
    {
        return $this->printFooterFile;
    }

    public function getAbsolutePrintFooterPath()
    {
        return null === $this->printFooterPath
            ? null
            : $this->getUploadRootDir().'/'.$this->printFooterPath;
    }

    public function getWebFooterPath()
    {
        return null === $this->printFooterPath
            ? null
            : $this->getUploadDir().$this->printFooterPath;
    }

    public function printFooterUpload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getPrintFooterFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getPrintFooterFile()->getClientOriginalName();
        $this->getPrintFooterFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->printFooterPath = $filename ;

        // clean up the file property as you won't need it anymore
        $this->printFooterFile = null;
    }


    /**
     * @return string
     */
    public function getNavbar()
    {
        return $this->navbar;
    }

    /**
     * @param string $navbar
     */
    public function setNavbar($navbar)
    {
        $this->navbar = $navbar;
    }

}
