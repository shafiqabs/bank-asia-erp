<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Profile;
use App\Entity\Admin\Setting;
use App\Entity\Core\UserReporting;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\BundleRoleGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="core_user")
 * @UniqueEntity(fields={"username"}, message="This employee user name must be unique")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class User implements UserInterface, \Serializable
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $reportTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $relieverTo;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\UserReporting", mappedBy="user")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $userReporting;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\UserReporting", mappedBy="reportTo")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $userReportingTo;

    /**
     * @var ApprovalUser
     * @ORM\OneToOne(targetEntity="App\Entity\Domain\ApprovalUser", mappedBy="user")
     */
    protected $approvalUser;

    /**
     * @var BundleRoleGroup
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\BundleRoleGroup", inversedBy="users")
     **/
    protected $userGroupRole;

    /**
     * @var BundleRoleGroup
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\BundleRoleGroup")
     */
    protected $approvalRole;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\UserReporting", mappedBy="relieverTo")
     */
    protected $userRelieverTo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\ItemKeyValue", mappedBy="user")
     */
    protected $itemKeyValues;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\Profile", mappedBy="user")
     */
    protected $profile;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Terminal", inversedBy="users",cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $terminal;




    /**
     * @var Location
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     */
    private $location;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $userGroup;


    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(min=2, max=200)
     */
    private $username;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Email()
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $otp;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isDomain = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $approveUser= false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];


    /**
     * @var array
     *
     * @ORM\Column(type="json" , nullable=true)
     */
    private $appRoles = [];


    public function getId()
    {
        return $this->id;
    }


    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        $role = $this->getRoles();
        return $role[0];

    }

    public function isGranted($role)
    {
        $domain = $this->getRole();
        if('ROLE_SUPER_ADMIN' === $domain or 'ROLE_DOMAIN' === $domain) {
            return true;
        }elseif(in_array($role, $this->getRoles())){
            return true;
        }
        return false;
    }



    public function hasRoles($role)
    {
        $array = array_intersect($role, $this->getRoles());
        if(!empty($array)){
            return true;
        }
        return false;
    }

    public function hasProcessRoles($roles)
    {
        $array = array_intersect($roles, $this->getRoles());
        if(!empty($array)){
            return true;
        }
        return false;
    }


    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        // See "Do you need to use a Salt?" at https://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one

        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        return serialize([$this->id, $this->username, $this->password]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        [$this->id, $this->username, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }


    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }


    /**
     * @return string
     */
    public function getOtp()
    {
        return $this->otp;
    }

    /**
     * @param string $otp
     */
    public function setOtp($otp)
    {
        $this->otp = $otp;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameWithEmployeeId()
    {
        $employeeId = empty($this->getProfile()) ? "" : $this->getProfile()->getEmployeeId();
        $name  = $this->name;
        if(empty($this->getProfile())){
            return $this->name;
        }else{
            $empl = "{$employeeId} - {$this->name}";
            return  $empl;
        }

    }

    /**
     * @return string
     */
    public function getNameEmployeeIdDesignation()
    {
        return $this->name;
    }


    /**
     * @return array
     */
    public function getAppRoles(): ? array
    {
        $appRoles = $this->appRoles;

        // guarantees that a user always has at least one role for security
        if (empty($appRoles)) {
            $appRoles[] = 'ROLE_USER';
        }
        return array_unique($appRoles);
    }



     /**
     * @param string $appRoles
     */
    public function setAppRoles(array $appRoles) : void
    {
        $this->appRoles = $appRoles;
    }


    /**
     * @return bool
     */
    public function isDelete(): ? bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete)
    {
        $this->isDelete = $isDelete;
    }


    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return Terminal
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param Terminal $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }


    /**
     * @return Location
     */
    public function getDistrict(){
        return $this->district;
    }

    /**
     * @param Location $district
     */
    public function setDistrict(Location $district)
    {
        $this->district = $district;
    }


    /**
     * @param string $checkBundle
     */
    public function checkBundle($bundles)
    {
        $terminal = $this->getTerminal();
        $modules = $terminal->getAppBundles();
        $arrSlugs = array();
        if (!empty($terminal->getAppBundles()) and !empty($modules)) {
            foreach ($terminal->getAppBundles() as $mod) {
                if (!empty($mod->getModuleClass())) {
                    $arrSlugs[] = $mod->getSlug();
                }
            }
        }
        $result = array_intersect($arrSlugs, $bundles);
        if(!empty($result)){
            return true;
        }
        return false;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return UserReporting
     */
    public function getUserReporting()
    {
        return $this->userReporting;
    }

    /**
     * @return UserReporting
     */
    public function getUserReportingTo()
    {
        return $this->userReportingTo;
    }

    /**
     * @return UserReporting
     */
    public function getUserRelieverTo()
    {
        return $this->userRelieverTo;
    }

    /**
     * @return BundleRoleGroup
     */
    public function getUserGroupRole()
    {
        return $this->userGroupRole;
    }

    /**
     * @param BundleRoleGroup $userGroupRole
     */
    public function setUserGroupRole($userGroupRole)
    {
        $this->userGroupRole = $userGroupRole;
    }

    /**
     * @return ApprovalUser
     */
    public function getApprovalUser()
    {
        return $this->approvalUser;
    }

    /**
     * @return string
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }

    /**
     * @param string $userGroup
     */
    public function setUserGroup($userGroup)
    {
        $this->userGroup = $userGroup;
    }


    /**
     * @return bool
     */
    public function isDomain()
    {
        return $this->isDomain;
    }

    /**
     * @param bool $isDomain
     */
    public function setIsDomain(bool $isDomain)
    {
        $this->isDomain = $isDomain;
    }

    /**
     * @return
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param  $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getRelieverTo()
    {
        return $this->relieverTo;
    }

    /**
     * @param mixed $relieverTo
     */
    public function setRelieverTo($relieverTo)
    {
        $this->relieverTo = $relieverTo;
    }

    /**
     * @return BundleRoleGroup
     */
    public function getApprovalRole()
    {
        return $this->approvalRole;
    }

    /**
     * @param BundleRoleGroup $approvalRole
     */
    public function setApprovalRole($approvalRole)
    {
        $this->approvalRole = $approvalRole;
    }

    public function isApproveGranted($role)
    {
        $approve = $this->getUserGroupRole();
        if($approve and  $approve->getRoleName() == $role){
            return true;
        }
        return false;
    }

    public function approveGrantedUsers($roles)
    {
        $approve = $this->getUserGroupRole();
        if($approve and  in_array($approve->getRoleName(),$roles)){
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isApproveUser()
    {
        return $this->approveUser;
    }

    /**
     * @param bool $approveUser
     */
    public function setApproveUser($approveUser)
    {
        $this->approveUser = $approveUser;
    }

    /**
     * @return BundleRoleGroup
     */
    public function getUserAccessRole()
    {
        return $this->userAccessRole;
    }

    /**
     * @param BundleRoleGroup $userAccessRole
     */
    public function setUserAccessRole(BundleRoleGroup $userAccessRole)
    {
        $this->userAccessRole = $userAccessRole;
    }


}
