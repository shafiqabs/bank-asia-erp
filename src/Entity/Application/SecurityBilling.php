<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Application;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\NbrvatBundle\Entity\TaxSetup;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Application\SecurityBillingRepository")
 * @ORM\Table(name="tbd_security_billing")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SecurityBilling
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $terminal;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $apiKey;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $apiValue;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isApi", type="boolean", nullable=true)
     */
    private $isApi = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return bool
     */
    public function isStatus(): ? bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return bool
     */
    public function isApi()
    {
        return $this->isApi;
    }

    /**
     * @param bool $isApi
     */
    public function setIsApi($isApi)
    {
        $this->isApi = $isApi;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey( $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiValue()
    {
        return $this->apiValue;
    }

    /**
     * @param string $apiValue
     */
    public function setApiValue($apiValue)
    {
        $this->apiValue = $apiValue;
    }




}
