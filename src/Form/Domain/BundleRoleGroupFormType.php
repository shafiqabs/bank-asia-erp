<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Domain;


use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Entity\Domain\BundleRoleGroup;
use App\Entity\Domain\ModuleProcess;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Console\Terminal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BundleRoleGroupFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $bundle =  $options['terminal']->getAppBundles();
        $builder
            ->add('appBundle', EntityType::class, [
                'class' => AppBundle::class,
                'multiple' => false,
                'choice_label'  => 'name',
                'attr'=>['class'=>'select2'],
                'placeholder' => 'Choose a app bundle',
                'query_builder' => function(EntityRepository $er)  use($bundle){
                    return $er->createQueryBuilder('e')
                        ->where('e.id IN (:bundle)')->setParameter('bundle', $bundle)
                        ->orderBy('e.name', 'ASC');
                },
            ])

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                'required' => true,
            ])

            ->add('roleGroup', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'User' => 'User',
                    'Approve' => 'Approve'
                ]
            ])


            ->add('roleName', ChoiceType::class, [
                'multiple' => false,
                'required' => false,
                'choices'   => $options['userRepo']->getAccessRoleGroup($options['terminal'])
            ])

            ->add('process', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                'required' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'required' => false,
                'choices'   => $options['userRepo']->getAccessRoleGroup($options['terminal'])
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BundleRoleGroup::class,
            'terminal' => Terminal::class,
            'userRepo' => UserRepository::class,
            //'userRepo' => '',
        ]);
    }
}
