<?php

namespace App\Form\Domain;

use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Setting;
use App\Entity\Domain\ApprovalFinancialMatrix;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\Branch;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;




/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ApprovalUserFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal = $options['terminal']->getId();
        $bundle =  $options['terminal']->getAppBundles();
        $builder

            ->add('user', EntityType::class, array(
                'required'    => true,
                'class' => User::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose a user name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.enabled =1")
                        ->andWhere("e.approveUser =1")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('departments', EntityType::class, array(
                'required'    => true,
                'multiple'    => true,
                'expanded' => true,
                'class' => Setting::class,
                'placeholder' => 'Choose a  Designation',
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
             ->add('branches', EntityType::class, array(
                'required'    => true,
                'multiple'    => true,
                'expanded' => true,
                'class' => Branch::class,
                'placeholder' => 'Choose a  Branch',
                'choice_label' => 'name',
                'label' => 'Company',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.branchType ='branch'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->andWhere('e.isDelete IS NULL')
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('appModules', EntityType::class, [
                'class' => AppModule::class,
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'group_by'  => 'appBundle.name',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'placeholder' => 'Choose a app module',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($bundle){
                    return $er->createQueryBuilder('e')
                        ->join('e.appBundle','b')
                        ->where('b.id IN (:bundle)')->setParameter('bundle', $bundle)
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('financialMatrixCapex', EntityType::class, [
                'class' => ApprovalFinancialMatrix::class,
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder' => 'Choose a approve capex amount',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('financialMatrixOpex', EntityType::class, [
                'class' => ApprovalFinancialMatrix::class,
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder' => 'Choose a approve opex amount',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('financialMatrixExpense', EntityType::class, [
                'class' => ApprovalFinancialMatrix::class,
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'placeholder' => 'Choose a approve expense amount',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('process', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
            ])
            ->add('isMandatory',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Yes",
                    'data-off'=> "No"
                ],
            ])


            ->add('isRejected',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "danger",
                    'data-onstyle'=> "info",
                    'data-on' => "Yes",
                    'data-off'=> "No"
                ],
            ])

            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ApprovalUser::class,
            'terminal' => Terminal::class,
        ]);
    }
}
