<?php
namespace App\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class PaginatorService
{

    private $em;
    private $paginator;
    protected $requestStack;

    public function __construct(EntityManager $em, $paginator,RequestStack $requestStack)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->requestStack = $requestStack;
    }

    public function paginate($query, $pageLimit = 1, $pageNumber)
    {
        $request = $this->requestStack->getCurrentRequest();
        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', $pageNumber),
            $pageLimit
        );
        return $pagination;
    }

}