<?php

namespace App\Service;


class SwiftMailer
{

    public function sendMail()
    {
        $username = $_ENV['MAIL_USERNAME'];
        $password = $_ENV['MAIL_PASSWORD'];
        $mailHost = $_ENV['MAIL_HOST'];
        $mailPort = $_ENV['MAIL_PORT'];
        $mailEncryption = $_ENV['MAIL_ENCRYPTION'];
        $transport = (new \Swift_SmtpTransport($mailHost, $mailPort,$mailEncryption))
            ->setUsername($username)
            ->setPassword($password);
        $mailer = new \Swift_Mailer($transport);
        $message = (new \Swift_Message('Wonderful Subject'))
            ->setFrom(['shafiqbd.official@gmail.com' => 'John Doe'])
            ->setTo(['terminalbd@gmail.com'=>'B Name','shafiqabs@gmail.com' => 'A name'])
            ->setBody('Here is the message itself Swift Mailer is a component based library for sending e-mails from PHP applications.')
        ;
        $mailer->send($message);

    }

} 