<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventSubscriber;

use App\Event\EmailEvent;
use App\Event\EmailForgetPasswordEvent;
use App\Service\EasyMailer;
use Complex\Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Notifies post's author about new comments.
 *
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 */
class EmailForgetPasswordSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $translator;
    private $urlGenerator;
    private $sender;
    private $senderName;
    /** @var EasyMailer */
    protected $easymailer;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $urlGenerator, TranslatorInterface $translator,EasyMailer $easymailer, $sender,$senderName)
    {
        $this->mailer = $mailer;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        $this->sender = $sender;
        $this->senderName = $senderName;
        $this->easymailer = $easymailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            EmailForgetPasswordEvent::class => 'onCommentCreated',
        ];
    }

    public function onCommentCreated(EmailForgetPasswordEvent $event): void
    {

        $emailData = $event->getEmailData();
        $link = $emailData['link'];
        $invoice = $emailData['invoice'];
        $created = $emailData['created'];
        $toName = $emailData['toName'];
        $toEmail = $emailData['toEmail'];
        try {
            $mailer = $this->easymailer->mailer();
            $message = (new \Swift_Message('Reset Password'))
                ->setFrom([$this->sender => $this->senderName])
                ->setTo(['imammahadi.ovi@gmail.com' => 'Imam Mahadi', 'terminalbd@gmail.com' => 'Md Shafiqul Islam'])
                ->setBody('After creating the transport and mailer objects, the only remaining thing is to instantiate the Swift_Message object and decorate it with the necessary attributes.', 'text/html');
            // Set the "To address" [Use setTo method for multiple recipients, argument should be array]
            //$message->addTo('terminalbd@gmail.com','recipient name');

            // Add "CC" address [Use setCc method for multiple recipients, argument should be array]
            //$message->addCc('recipient@gmail.com', 'recipient name');

            // Add "BCC" address [Use setBcc method for multiple recipients, argument should be array]
            //$message->addBcc('recipient@gmail.com', 'recipient name');
            // Add an "Attachment" (Also, the dynamic data can be attached)
            //   $attachment = Swift_Attachment::fromPath('example.xls');
            //   $attachment->setFilename('report.xls');
            //  $message->attach($attachment);

            // Add inline "Image"
            //    $inline_attachment = Swift_Image::fromPath('nature.jpg');
            //      $cid = $message->embed($inline_attachment);

            $mailer->send($message);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
