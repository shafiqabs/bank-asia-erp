<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Application;

use App\Entity\Application\Accounting;
use App\Entity\Application\Procurement;
use App\Entity\Domain\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\DeliveryBatch;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\OrderDelivery;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcessLevel;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Form\Bank\TenderPreparetionFormType;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Procurement::class);
    }

    public function config($terminal)
    {
        $config = $this->findOneBy(array('terminal' => $terminal));
        return $config;
    }

    public function remove($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {
            $qb = $em->createQueryBuilder();
            $config = $con->getId();

            $ProcurementProcess = $qb->delete(ProcurementProcess::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($ProcurementProcess){
                $ProcurementProcess->execute();
            }

            $OrderDelivery = $qb->delete(OrderDelivery::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($OrderDelivery){
                $OrderDelivery->execute();
            }

            $TenderConditionItem = $qb->delete(TenderConditionItem::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($TenderConditionItem){
                $TenderConditionItem->execute();
            }

            $ProcurementCondition = $qb->delete(ProcurementCondition::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($ProcurementCondition){
                $ProcurementCondition->execute();
            }


            $TenderWorkorderReceive = $qb->delete(TenderWorkorderReceive::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($TenderWorkorderReceive){
                $TenderWorkorderReceive->execute();
            }

            $TenderWorkorder = $qb->delete(TenderWorkorder::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($TenderWorkorder){
                $TenderWorkorder->execute();
            }

            $TenderComparative = $qb->delete(TenderComparative::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($TenderComparative){
                $TenderComparative->execute();
            }


            $Tender = $qb->delete(Tender::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Tender){
                $Tender->execute();
            }

            $RequisitionOrder = $qb->delete(RequisitionOrder::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($RequisitionOrder){
                $RequisitionOrder->execute();
            }

            $RequisitionIssue = $qb->delete(RequisitionIssue::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($RequisitionIssue){
                $RequisitionIssue->execute();
            }

            $requisition = $qb->delete(Requisition::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($requisition){
                $requisition->execute();
            }

            $JobRequisition = $qb->delete(JobRequisition::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($JobRequisition){
                $JobRequisition->execute();
            }

            $Particular = $qb->delete(Particular::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Particular){
                $Particular->execute();
            }

            //  $inventoryReturn = $em->createQuery("DELETE TerminalbdProcurementBundle:Requisition e WHERE e.config = '{$config}'");
            // $inventoryReturn->execute();

        }
        if(!empty($con) and $process == "remove"){
            $em->remove($con);
            $em->flush();
        }

    }

    public function reset($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();


            $ProcurementProcess = $qb->delete(ProcurementProcess::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($ProcurementProcess){
                $ProcurementProcess->execute();
            }

            $tenderReceive = $qb->delete(TenderWorkorderReceive::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($tenderReceive){
                $tenderReceive->execute();
            }

            $tenderWorkorder = $qb->delete(TenderWorkorder::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($tenderWorkorder){
                $tenderWorkorder->execute();
            }

            $tenderComparative = $qb->delete(TenderComparative::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($tenderComparative){
                $tenderComparative->execute();
            }

            $TenderMemo = $qb->delete(TenderMemo::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($TenderMemo){
                $TenderMemo->execute();
            }

            $tender = $qb->delete(Tender::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($tender){
                $tender->execute();
            }

            $TenderBatch = $qb->delete(TenderBatch::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($TenderBatch){
                $TenderBatch->execute();
            }

            $RequisitionOrder = $qb->delete(RequisitionOrder::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($RequisitionOrder){
                $RequisitionOrder->execute();
            }

            $RequisitionIssue = $qb->delete(RequisitionIssue::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($RequisitionIssue){
                $RequisitionIssue->execute();
            }

            $DeliveryBatch = $qb->delete(DeliveryBatch::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($DeliveryBatch){
                $DeliveryBatch->execute();
            }

            $requisition = $qb->delete(Requisition::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($requisition){
                $requisition->execute();
            }

        }

    }



}
