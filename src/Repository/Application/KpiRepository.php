<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Application;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Kpi;
use App\Entity\Application\Nrbvat;
use App\Entity\Application\Production;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class KpiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kpi::class);
    }

    public function config($terminal)
    {
        $config = $this->findOneBy(array('terminal' => $terminal));
        return $config;
    }

    public function reset(Terminal $terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {
            $config = $con->getId();
            $inventoryReturn = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionInventoryReturn e WHERE e.config = '{$config}'");
            $inventoryReturn->execute();
            $receiveBatch = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionReceiveBatch e WHERE e.config = '{$config}'");
            $receiveBatch->execute();
            $batch = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionBatch e WHERE e.config = '{$config}'");
            $batch->execute();
            $order = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionWorkOrder e WHERE e.config = '{$config}'");
            $order->execute();
            $receiveBatch = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionItemAmendment e WHERE e.config = '{$config}'");
            $receiveBatch->execute();
            $item = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionItem e WHERE e.config = '{$config}'");
            $item->execute();
            $inventory = $em->createQuery("DELETE TerminalbdProductionBundle:ProductionInventory e WHERE e.config = '{$config}'");
            $inventory->execute();
        }
        if($process == "remove"){
            $em->remove($con);
            $em->flush();
        }

    }

}
