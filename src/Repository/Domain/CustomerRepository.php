<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Customer;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\SecurityBillingBundle\Entity\DeploymentPost;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $customer = $qb->delete(Customer::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($customer){
            $customer->execute();
        }
    }

    public function checkAvailable($terminal , $mode,$data)
    {
        $process = "true";
        $mobile             = isset($data['mobile']) ? $data['mobile'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->where('e.terminal =:terminal')->setParameter("terminal",$terminal);
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$form)
    {
        if(isset($form['filter_form'])){
            $data = $form['filter_form'];
            $name = isset($data['name'])? $data['name'] :'';
            $mobile = isset($data['mobile'])? $data['mobile'] :'';
            $shotName = !empty($data['shortName'])? $data['shortName'] :'';
            $keyword = isset($data['keyword'])? $data['keyword'] :'';
            if(!empty($name)){
                $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
            }
            if(!empty($mobile)){
                $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
            }
            if(!empty($shotName)){
                $qb->andWhere($qb->expr()->like("e.shortName", "'%$shotName%'"));
            }
            if (!empty($keyword)) {
                $qb->andWhere('e.name LIKE :searchTerm OR e.shortName LIKE :searchTerm OR e.mobile LIKE :searchTerm  OR e.address LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$keyword.'%');
            }
        }
    }

    public function removeBranch(Customer $entity)
    {
        $qb = $this->_em->createQueryBuilder();
        $q = $qb->update(Branch::class, 'e')
            ->set('e.isDelete', '?1')->setParameter(1,true)
            ->set('e.status', '?2')->setParameter(2,false)
            ->where('e.customer = ?3')
            ->setParameter(3, $entity->getId())
            ->getQuery();
        $q->execute();
        $q = $qb->update(DeploymentPost::class, 'e')
            ->set('e.isDelete', '?1')->setParameter(1,true)
            ->set('e.status', '?2')->setParameter(2,false)
            ->where('e.customer = ?3')
            ->setParameter(3, $entity->getId())
            ->getQuery();
        $q->execute();
    }
    public function restoreBranch(Customer $entity)
    {
        $qb = $this->_em->createQueryBuilder();
        $q = $qb->update(Branch::class, 'e')
            ->set('e.isDelete', '?1')->setParameter(1,false)
            ->set('e.status', '?2')->setParameter(2,true)
            ->where('e.customer = ?3')
            ->setParameter(3, $entity->getId())
            ->getQuery();
        $q->execute();
        $q = $qb->update(DeploymentPost::class, 'e')
            ->set('e.isDelete', '?1')->setParameter(1,false)
            ->set('e.status', '?2')->setParameter(2,true)
            ->where('e.customer = ?3')
            ->setParameter(3, $entity->getId())
            ->getQuery();
        $q->execute();
    }


    public function systemDefaultCustomer( Terminal $terminal){

        $em = $this->_em;
        $user = new Customer();
        $user->setName("Default");
        $user->setMobile($terminal->getMobile());
        if($terminal->getEmail()){
            $user->setEmail($terminal->getEmail());
        }
        $em->persist($user);
        $em->flush();
    }


    /**
     * @return Customer[]
     */

    public function findBySearchQuery( $terminal, $data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.name';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id as id','e.name as name','e.shortName as shortName','e.mobile as mobile','e.email as email','e.billingAddress as address','e.status as status');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('e.isDelete IS NULL');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }



}
