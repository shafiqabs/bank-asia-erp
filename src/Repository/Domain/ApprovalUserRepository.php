<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\Requisition;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ApprovalUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApprovalUser::class);
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $ApprovalUser = $qb->delete(ApprovalUser::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($ApprovalUser){
            $ApprovalUser->execute();
        }
    }

    public function getGroupModules($terminal):array
    {


        /** @var $module ModuleProcess */

        $qb = $this->_em->createQueryBuilder();
        $qb->from(AppModule::class,'e');
        $qb->join('e.approvalUser', 'au');
        $qb->join('au.user', 'u');
        $qb->join('u.profile', 'p');
        $qb->leftJoin('p.department', 'd');
        $qb->leftJoin('p.designation', 'dg');
        $qb->select('e.id as moduleId','e.name as module');
        $qb->addSelect('u.id as uid','u.name as name');
        $qb->addSelect('dg.name as designation');
        $qb->addSelect('d.name as department');
        $qb->addSelect('au.id as id','au.process as process','au.ordering as ordering','au.isMandatory as isMandatory','au.isRejected as isRejected','au.status as status');
        $qb->where('au.terminal = :terminal')->setParameter('terminal', $terminal);
        $result = $qb->getQuery()->getArrayResult();
        $arries = array();
        foreach ($result as $row){
            $arries[$row['moduleId']][] = $row;
        }
        return $arries;
    }

    public function getDepartmentApprovalAssignUser($terminal ,$requisition)
    {

        if(!empty($requisition->getModule()) and !empty($requisition->getDepartment())){
            $module = $requisition->getModule();
            $department = $requisition->getDepartment()->getId();
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->join('e.departments','d');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if($requisition->getModule()){
                $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
            }
            if($requisition->getDepartment()){
                $qb->andWhere('d.id = :id')->setParameter('id',$department);
            }
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getApprovalAssignUser($terminal ,$requisition)
    {
        /** @var $module ModuleProcess */
        $module = $requisition->getModuleProcess();
        if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->leftJoin('e.departments','d');
            $qb->leftJoin('e.branches','br');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            $qb->andWhere('e.status = 1');
            if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
                $qb->andWhere('b.id = :mid')->setParameter('mid',$module->getModule()->getId());
            }
            if(!empty($module->getOperationGroup()) and $module->getOperationGroup() == 'department' and !empty($requisition->getDepartment())){
                $department = $requisition->getDepartment()->getId();
                $qb->andWhere('d.id = :did')->setParameter('did',$department);
            }elseif(!empty($module->getOperationGroup()) and $module->getOperationGroup() == 'branch' and !empty($requisition->getBranch())){
                $branch = $requisition->getBranch()->getId();
                $qb->andWhere('br.id = :bid')->setParameter('bid',$branch);
            }
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getApprovalAssignMatrixUser($terminal , Requisition $requisition)
    {
        /** @var $module ModuleProcess */
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user', 'u');
        // $qb->leftJoin('e.appModules', 'b');
        $qb->leftJoin('e.departments', 'd');
        $qb->leftJoin('e.branches', 'br');
        $qb->select('fm.amount');
        $qb->addSelect('u.id as uid');
        $qb->addSelect('e.process as process','e.ordering as ordering');
        $qb->where('e.terminal = :terminal')->setParameter('terminal', $terminal);
        $department = $requisition->getProcessDepartment()->getId();
        $qb->andWhere('d.id = :did')->setParameter('did', $department);
        $branch = $requisition->getCompanyUnit()->getParent()->getId();
        $qb->andWhere('br.id = :bid')->setParameter('bid', $branch);
        if ($requisition->getRequisitionMode() == "CAPEX"){
            $qb->join('e.financialMatrixCapex', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "OPEX"){
            $qb->join('e.financialMatrixOpex', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "Expense"){
            $qb->join('e.financialMatrixExpense', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }
        $qb->orderBy('e.ordering','ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function getCheckModule($terminal , $module)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->join('e.appModules','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($module){
            $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
        }
        $result = $qb->getQuery()->getResult();
        return $result;

    }

     public function getModuleApprovalAssignUser($terminal , $module = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->join('e.appModules','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($module){
            $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
        }
        $result = $qb->getQuery()->getResult();
        return $result;

    }

    public function getModuleApprovalUser($terminal , $bundle = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.appModules','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($bundle){
            $qb->andWhere('b.slug = :slug')->setParameter('slug',$bundle);
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }
}
