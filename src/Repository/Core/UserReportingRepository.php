<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;


use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use App\Entity\Core\UserReporting;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class UserReportingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserReporting::class);
    }

    public function insertUser(User $user){
        $em=$this->_em;

        $userReporting = new UserReporting();
        $userReporting->setUser($user);
        $em->persist($userReporting);
        $em->flush();
    }

    public function reportTo(User $user)
    {

        if(!empty($user->getUserReporting())){
            if(!empty($user->getUserReporting()->getReportTo()) and $user->getUserReporting()->isStatus() == true){
                return $report = $user->getUserReporting()->getReportTo();
            }elseif(!empty($user->getUserReporting()->getRelieverTo()) and $user->getUserReporting()->isStatus() == false) {;
                return  $report = $user->getUserReporting()->getRelieverTo();
            }
        }
        return '';
    }
}
