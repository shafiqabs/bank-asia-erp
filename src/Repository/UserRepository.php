<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;


use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\BundleRoleGroup;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $User = $qb->delete(User::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($User){
            $User->execute();
        }
    }

    protected function handleSearchBetween($qb,$form)
    {

        if (isset($form['filter_form'])) {
            $data = $form['filter_form'];
            $employeeId = isset($data['employeeId']) ? $data['employeeId'] : '';
            $name = isset($data['name']) ? $data['name'] : '';
            $username = isset($data['username']) ? $data['username'] : '';
            $email = isset($data['email']) ? $data['email'] : '';
            $mobile = isset($data['mobile']) ? $data['mobile'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $designation = isset($data['designation']) ? $data['designation'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $reportTo = isset($data['reportTo']) ? $data['reportTo'] : '';
            $relieverTo = isset($data['relieverTo']) ? $data['relieverTo'] : '';
            if (!empty($employeeId)) {
                $qb->andWhere($qb->expr()->like("p.employeeId", "'%$employeeId%'"));
            }
            if (!empty($reportTo)) {
                $qb->andWhere('rt.id =:report')->setParameter('report', $reportTo);
            }
            if (!empty($relieverTo)) {
                $qb->andWhere('rl.id =:report')->setParameter('report', $relieverTo);
            }
            if (!empty($branch)) {
                $qb->andWhere('b.id =:branch')->setParameter('branch', $branch);
            }
            if (!empty($department)) {
                $qb->andWhere('dp.id =:department')->setParameter('department', $department);
            }
            if (!empty($designation)) {
                $qb->andWhere('dg.id =:designation')->setParameter('designation', $designation);
            }
            if (!empty($name)) {
                $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
            }
            if (!empty($username)) {
                $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
            }
            if (!empty($email)) {
                $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
            }
            if (!empty($mobile)) {
                $qb->andWhere($qb->expr()->like("p.mobile", "'%$mobile%'"));
            }
        }

    }

    public function systemuserCreate( Terminal $terminal, UserPasswordEncoderInterface $encoder ){

            $em = $this->_em;
            $user = new User();
            $user->setTerminal($terminal);
            $user->setName($terminal->getOrganizationName());
            $user->setUsername($terminal->getMobile());
            $user->setMobile($terminal->getMobile());
            if($terminal->getEmail()){
                $user->setEmail($terminal->getEmail());
            }else{
                $email = $terminal->getMobile()."@gmail.com";
                $user->setEmail($email);
            }
            $user->setPassword($encoder->encodePassword($user,"*148148#"));
            $user->setRoles(array('ROLE_DOMAIN'));
            $em->persist($user);
            $em->flush();
    }

    /**
     * @return User[]
     */
    public function findWithSearchQuery($domain ,$data )
    {
        $sort = isset($data['sort']) ? $data['sort'] : 'p.employeeId';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.profile','p');
        $qb->leftJoin('p.designation','dg');
        $qb->leftJoin('p.department','dp');
        $qb->leftJoin('p.branch','b');
        $qb->leftJoin('e.reportTo','rt');
        $qb->select('e.id as id','e.name as name','e.email as email','e.username as username','e.enabled as enabled');
        $qb->addSelect('p.mobile as mobile','p.employeeId as employeeId');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('dg.name as designation');
        $qb->addSelect('dp.name as department');
        $qb->addSelect('rt.name as reportTo');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
        $qb->andWhere("e.isDelete IS NULL")->orWhere("e.isDelete = 0");
        $qb->andWhere('e.userGroup =:group')->setParameter('group',"employee");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}", "$direction");
        $result = $qb->getQuery();
        return $result;
    }

    /**
     * @return User[]
     */
    public function findEmployeeReporting($terminal , $data): array
    {

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.profile','p');
        $qb->leftJoin('p.designation','dg');
        $qb->leftJoin('p.department','dp');
        $qb->leftJoin('p.branch','b');
        $qb->leftJoin('e.reportTo','rt');
        $qb->leftJoin('e.relieverTo','rl');
        $qb->select('e.id as id','e.name as name');
        $qb->addSelect('p.mobile as mobile','e.enabled as enabled','p.employeeId as employeeId');
        $qb->addSelect('dg.name as designationName');
        $qb->addSelect('dp.name as departmentName');
        $qb->addSelect('rt.id as reportingId');
        $qb->addSelect('rt.name as reportingName');
        $qb->addSelect('rl.id as relieverId');
        $qb->addSelect('rl.name as relieverName');
        $qb->addSelect('b.name as branchName');
        $qb->where('e.terminal =:terminal')->setParameter( 'terminal',$terminal);
        $qb->andWhere('e.userGroup =:uGroup')->setParameter('uGroup','employee');
        $this->handleSearchBetween($qb,$data);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    /**
     * @return User[]
     */
    public function findBySearchQuery($domain , $parameter , $data ): array
    {

        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.profile','p');
        $qb->leftJoin('p.designation','dg');
        $qb->leftJoin('p.department','dp');
        $qb->leftJoin('e.userGroup','ug');
        $qb->leftJoin('p.branch','b');
        $qb->select('e.id as id','e.name as name','e.email as email','e.username as username');
        $qb->addSelect('p.mobile as mobile','e.enabled as enabled');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('dg.name as designation');
        $qb->addSelect('dp.name as department');
        $qb->addSelect('ug.name as userGroup');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
    //    $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    /**
     * @return User[]
     */
    public function findUserReporting($domain , $parameter, $data): array
    {
        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.profile','p');
        $qb->leftJoin('p.designation','dg');
        $qb->leftJoin('p.department','dp');
        // $qb->leftJoin('p.branch','b');
        $qb->leftJoin('e.userGroup','ug');
       // $qb->leftJoin('e.userReporting','ur');
        $qb->leftJoin('e.reportTo','rt');
        $qb->leftJoin('e.relieverTo','rl');
        $qb->select('e.id as id','e.name as name');
        $qb->addSelect('p.mobile as mobile','e.enabled as enabled');
        $qb->addSelect('dg.name as designationName');
        $qb->addSelect('dp.name as departmentName');
        $qb->addSelect('rt.id as reportingId');
        $qb->addSelect('rt.name as reportingName');
        $qb->addSelect('rl.id as relieverId');
        $qb->addSelect('rl.name as relieverName');
        //  $qb->addSelect('b.name as branchName');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
        $qb->andWhere('ug.slug =:uGroup')->setParameter('uGroup','general');
        //    $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function findUserAssignReporting($domain,$id)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.profile','p');
        $qb->select('e.id as id');
        $qb->addSelect('e.name as name','p.employeeId as employeeId');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
        $qb->andWhere('e.userGroup =:uGroup')->setParameter('uGroup','employee');
        $qb->andWhere("e.id != {$id}");
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function findUserForApproval($domain,$users)
    {
        $ids = [];
        foreach ($users as $user){
          $ids[] = $user->getUser()->getId();
        }
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.profile','p');
        $qb->select('e.id as id');
        $qb->addSelect('e.name as name');
        $qb->addSelect('e.userGroup as ugName');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
        $qb->andWhere('p.approvalUser =1');
        if($ids){
            $qb->andWhere($qb->expr()->notIn('e.id', $ids));
        }
        $qb->orderBy('e.id', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function getAccessRoleGroup(Terminal $terminal){

        $em = $this->_em;
        $modules = $terminal->getAppBundles();
        $arrSlugs = array();
        if (!empty($terminal->getAppBundles()) and !empty($modules)) {
            /* @var $mod AppBundle */
            foreach ($terminal->getAppBundles() as $mod) {
                if (!empty($mod->getModuleClass())) {
                    $arrSlugs[] = $mod->getSlug();
                }
            }
        }
        $array = array();
        $generic = array('generic');
        $result = array_intersect($arrSlugs, $generic);
        if (!empty($result)) {
            $array['Generic Master'] = array(
                'Generic Master'  => 'ROLE_GMB',
                'Manager'   => 'ROLE_GMB_MANAGER',
                'Officer'   => 'ROLE_GMB_OFFICER',
                'Admin'     => 'ROLE_GMB_ADMIN',
            );
        }

        $inventory = array('inventory');
        $result = array_intersect($arrSlugs, $inventory);
        if (!empty($result)) {
            $array['Inventory'] = array(
                'Inventory' => 'ROLE_INVENTORY',
                'Manager'   => 'ROLE_INVENTORY_MANAGER',
                'Officer'   => 'ROLE_INVENTORY_OFFICER',
                'Report'    => 'ROLE_INVENTORY_REPORT',
                'Admin'     => 'ROLE_INVENTORY_ADMIN',
            );
        }

        $procurement = array('procurement');
        $result = array_intersect($arrSlugs, $procurement);
        if (!empty($result)) {
            $array['Procurement'] = array(
                'Procurement'           => 'ROLE_PROCUREMENT',
                'Requisition'           => 'ROLE_PROCUREMENT_REQUISITION',
                'MOB'                   => 'ROLE_PROCUREMENT_INITIATOR',
                'HOB'                   => 'ROLE_PROCUREMENT_MANAGER',
                'Store Officer'         => 'ROLE_PROCUREMENT_STORE',
                'Store Issue'           => 'ROLE_PROCUREMENT_ORDER_ISSUE',
                'Procurement Initiator' => 'ROLE_PROCUREMENT_INITIATOR',
                'Tender'                => 'ROLE_PROCUREMENT_TENDER',
                'Work Order'            => 'ROLE_PROCUREMENT_WORKORDER',
                'Goods Receive'         => 'ROLE_PROCUREMENT_RECEIVE',
                'LSSD'                  => 'ROLE_PROCUREMENT_LSSD',
                'Requisition Assessment'=> 'ROLE_PROCUREMENT_ASSESSMENT',
                'Head of LSSD'          => 'ROLE_PROCUREMENT_LSSD_HEAD',
                'Head of Department'    => 'ROLE_PROCUREMENT_DEPARTMENT_HEAD',
                'Member of Secretary'   => 'ROLE_PROCUREMENT_MEMBER_SECRETARY',
                'Department Officer'    => 'ROLE_PROCUREMENT_DEPARTMENT_OFFICER',
                'Report'                => 'ROLE_PROCUREMENT_REPORT',
                'Admin'                 => 'ROLE_PROCUREMENT_ADMIN',
            );
        }

        $securityBilling = array('security-billing');
        $result = array_intersect($arrSlugs, $securityBilling);
        if (!empty($result)) {
            $array['Security-Billing'] = array(
                'SBS'           => 'ROLE_SBS',
                'SBS Manager'   => 'ROLE_SBS_MANAGER',
                'SBS Verified'    => 'ROLE_SBS_VERIFIED',
                'SBS Approved'    => 'ROLE_SBS_APPROVED',
                'SBS Report'    => 'ROLE_SBS_REPORT',
                'SBS Admin'     => 'ROLE_SBS_ADMIN',
            );
        }
        $securityBilling = array('crm');
        $result = array_intersect($arrSlugs, $securityBilling);
        if (!empty($result)) {
            $array['CRM'] = array(
                'CRM' => 'ROLE_CRM',
                'CSO' => 'ROLE_CRM_CSO',
                'Regional Manager' => 'ROLE_CRM_REGIONAL_MANAGER',
                'Zonal Manager' => 'ROLE_CRM_ZONAL_MANAGER',
                'Admin' => 'ROLE_CRM_ADMIN',
            );
        }
        $budget = array('budget');
        $result = array_intersect($arrSlugs, $budget);
        if (!empty($result)) {
            $array['Budget & Cost'] = array(
                'Budget' => 'ROLE_BUDGET',
                'Officer' => 'ROLE_BUDGET_OFFICER',
                'Report' => 'ROLE_BUDGET_REPORT',
                'Admin' => 'ROLE_BUDGET_ADMIN',
            );
        }

        $roles = $em->getRepository(BundleRoleGroup::class)->findBy(['status'=>1],['name'=>'ASC']);
        $arrRoles =[];
        foreach ($roles as $role):
            $arrRoles[$role->getName()] = $role->getRoleName();
        endforeach;
        $array['Administrator'] = array(
            'Administrator'   => 'ROLE_DOMAIN',
            'Approver'   => 'ROLE_APPROVER'
        );
        return $array;
    }

    public function getAndroidRoleGroup(){

        $array = array();
        $array['Android Apps'] = array(
            'Manager'                                   => 'ROLE_MANAGER',
            'Purchase'                                  => 'ROLE_PURCHASE',
            'Sales'                                     => 'ROLE_SALES',
            'Expense'                                   => 'ROLE_EXPENSE',
            'Stock'                                     => 'ROLE_STOCK',
        );


        return $array;
    }

}
