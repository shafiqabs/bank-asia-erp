$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
var origin   = window.location.origin;   // Returns base URL (https://example.com)


$( document ).ready(function( $ ) {

    $(document).on('click',".remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure you want to delete this record?"
        }).done(function(){
            $.get(url, function( data ) {
                $('#remove-'+id).hide();
                $(event.target).closest('tr').hide();
            });
        });
    });

    $(document).on('opened', '.remodal', function () {
        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });


    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

 /*   $('.hover').tooltip({
        title: fetchData,
        html: true,
        placement: 'right'
    });*/
});

function formCommonProcess() {

    $('.form-body').slimScroll({
        height: '400px'
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.mobileLocal').mask("00000-000000", {placeholder: "_____-______"});

    $('.checkboxToggle').bootstrapToggle();

    $('.multi-select2').multiSelect({ selectableOptgroup: true });
    $('#optgroup').multiSelect({ selectableOptgroup: true });
}

var explode = function AutoReload()
{
    $('#entityDatatable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
    $('.form-submit').html("Save & Continue").prop("disabled", false);
}

function formSubmitProcess() {

    $("#postForm").validate({

        rules: {
            "registration_form[name]": {required: true},
            "registration_form[mobile]": {required: true},
            "registration_form[username]": {
                required: true,
                remote:window.location.pathname+"username/available"
            },
            "registration_form[password][first]" : {
                minlength : 5
            },
            "registration_form[password][second]" : {
                minlength : 5,
                equalTo : "#registration_form_password_first"
            }
        },

        messages: {

            "registration_form[name]": "Enter user full name",
            "registration_form[mobile]": "Enter user mobile no",
            "registration_form[username]":{
                required: "Please enter your user name.",
                remote: jQuery.validator.format("{0} username is already in use!")
            }
        },
        submitHandler: function(form) {
            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $('form#postForm')[0].reset();
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

function formEditSubmitProcess() {

    $("#postForm").validate({

        rules: {
            "registration_form[name]": {required: true},
            "registration_form[mobile]": {required: true},
            "registration_form[username]": {
                required: true,
                remote:window.location.pathname+"editable/username/available"
            },
            "registration_form[password][first]" : {
                minlength : 5
            },
            "registration_form[password][second]" : {
                minlength : 5,
                equalTo : "#registration_form_password_first"
            }
        },

        messages: {
            "registration_form[name]": "Enter user full name",
            "registration_form[mobile]": "Enter user mobile no",
            "registration_form[username]":{
                required: "Please enter your user name.",
                remote: jQuery.validator.format("{0} username is already in use!")
            }
        },
        submitHandler: function(form) {

            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

function fetchData()
{
    var fetch_data = '';
    var element = $(this);
    var id = element.attr("id");
    $.ajax({
        url:"fetch.php",
        method:"POST",
        async: false,
        data:{id:id},
        success:function(data)
        {fetch_data = data;}
    });
    return fetch_data;
}

