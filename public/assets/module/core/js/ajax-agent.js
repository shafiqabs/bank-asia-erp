$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/core/agent/data-table", // ajax source
            'data': function(data){

                var name = $('#name').val();
                var type = $('#type').val();
                var location = $('#location').val();
                var mobile = $('#mobile').val();
                var email = $('#email').val();

                // Append to data
                //  data._token = CSRF_TOKEN;
                data.name = name;
                data.type = type;
                data.location = location;
                data.mobile = mobile;
                data.email = email;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'name' },
            { "name": 'type' },
            { "name": 'location' },
            { "name": 'mobile' },
            { "name": 'email' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [6], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#type').change(function(){
        dataTable.draw();
    });

    $('#location').keyup(function(){
        dataTable.draw();
    });

    $('#mobile').keyup(function(){
        dataTable.draw();
    });

    $('#email').change(function(){
        dataTable.draw();
    });

});

