
$( document ).ready(function( $ ) {

    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#purchasePrice').val(obj['purchasePrice']);
                $('#remaining').val(obj['remaining']);
                $('#unit').html(obj['unit']);
                $('#quantity').data('max',obj['remaining']);
            }
        })
    });


    $(document).on('click', '#addProduct', function() {

        var productId = $('#productId').val();
        var quantity = $('#quantity').val();
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter issue quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity,
            success: function (response) {
                $('#product').select2('open');
                $('#quantity').val('');
                $('#purchasePrice').val('');
                $('#remaining').val('');
                setTimeout( explode, 2000);
            }
        })
    });


    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

    var explode = function AutoReload()
    {
        $('#entityDatatable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    }


});




