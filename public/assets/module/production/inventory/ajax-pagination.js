$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

    //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "returning": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/production/inventory/data-table", // ajax source
            'data': function(data){
                var name = $('#name').val();
                var uom = $('#uom').val();
                var quantity = $('#quantity').val();
                var issueQuantity = $('#issueQuantity').val();
                var damageQuantity = $('#damageQuantity').val();
                var returnQuantity = $('#returnQuantity').val();
                var remaining = $('#remaining').val();


                // Append to data
                //  data._token = CSRF_TOKEN;

                data.name = name;
                data.uom = uom;
                data.quantity = quantity;
                data.issueQuantity = issueQuantity;
                data.damageQuantity = damageQuantity;
                data.returnQuantity = returnQuantity;
                data.remaining = remaining;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'name' },
            { "name": 'uom' },
            { "name": 'quantity' },
            { "name": 'issueQuantity' },
            { "name": 'damageQuantity' },
            { "name": 'returnQuantity' },
            { "remaining": 'remaining' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [8], "sClass":  "text-center"}
        ],
        "order": [
            [1, "desc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 8,
            "orderable": false
        },
            {
                "targets": 0,
                "orderable": false
            }],

    });
    $('#name').change(function(){
        dataTable.draw();
    });



});

