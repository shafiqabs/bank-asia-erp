$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[issueDate="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/production/work-order/data-table", // ajax source
            'data': function(data){

                var created = $('#created').val();
                var issueDate = $('#issueDate').val();
                var requisitionNo = $('#requisitionNo').val();
                var invoice = $('#invoice').val();
                var process = $('#process').val();

                data.created = created;
                data.issueDate = issueDate;
                data.requisitionNo = requisitionNo;
                data.invoice = invoice;
                data.process = process;
            }
        },
        'columns': [
            { "name": 'checkbox','orderable':false},
            { "name": 'id','orderable':false},
            { "name": 'created' },
            { "name": 'issueDate' },
            { "name": 'requisitionNo' },
            { "name": 'invoice' },
            { "name": 'process' },
            { "name": 'action','orderable':false,"sClass":  "text-center" }

        ],
        "order": [
            [1, "desc"]
        ]

    });
    $('#startDate').change(function(){
        dataTable.draw();
    });
    $('#endDate').change(function(){
        dataTable.draw();
    });
    $('#process').change(function(){
        dataTable.draw();
    });
    $('#issueDate').change(function(){
        dataTable.draw();
    });



});

