
$( document ).ready(function( $ ) {

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#price').val(obj['price']);
                $('#unit').html(obj['unit']);
            }
        })
    });

    $(document).on('click', '#addProduct', function() {

        var productId = $('#productId').val();
        var quantity = $('#quantity').val();
        var wastagePercent = $('#wastagePercent').val();
        var price = $('#price').val();
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter item quantity");
            $('#quantity').focus();
            return false;
        }
        if(price === ''){
            $.MessageBox("Please enter item price");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity+'&price='+price+'&wastagePercent='+wastagePercent,
            success: function (response) {
                $('#product').select2('open');
                $('#quantity').val('');
                $('#price').val('');
                $('#wastagePercent').val('');
                $('#remaining').val('');
                obj = JSON.parse(response);
                $('#materialQuantity').html(obj['quantity']);
                $('#subTotal').html(obj['subTotal']);
                $('#wasteQuantity').html(obj['wasteQuantity']);
                $('#wasteAmount').html(obj['wasteAmount']);
                $('#invoiceItems').html(obj['invoiceItems']);

            }
        })
    });

    $(document).on('change', '.action', function () {
        $.ajax({
            url:  $('form#postForm').attr('action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false
        });

    });


    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

    $(document).on('keyup', ".amount", function() {
        var sum = 0;
        $(".amount").each(function(){
            sum += + parseFloat($(this).val());
        });
        total = financial(sum);
        $("#total").html(total);
    });

    $(document).on('click',".item-remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                $(event.target).closest('tr').hide();
                obj = JSON.parse(response);
                $('#materialQuantity').html(obj['quantity']);
                $('#subTotal').html(obj['subTotal']);
                $('#wasteQuantity').html(obj['wasteQuantity']);
                $('#wasteAmount').html(obj['wasteAmount']);
                $('#invoiceItems').html(obj['invoiceItems']);
            });
        });
    });



});




