
$( document ).ready(function( $ ) {

    currentUrl = window.location.href;
    var segments = currentUrl.split( '/' );
    var lang = segments[3];

    $('.datePicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });

    $(document).on('click',".addVendor", function (event) {
        var url = $(this).attr('data-action');
        $.MessageBox({
            message : "<b>ADD VENDOR</b>",
            buttonDone      : {
                save : {
                    text        : "Save",
                    customClass : "custom_button",
                    keyCode     : 13
                }
            },
            buttonFail      : "Cancel",
            input   : {
                vendorType : {
                    type         : "select",
                    label        : "Vendor Type",
                    title        : "Select a vendor type",
                    options      : ["Local", "Foreign", "Production"]
                },
                businessType : {
                    type         : "select",
                    label        : "Vendor Business Type",
                    title        : "Select a vendor business type",
                    options      : ["General", "Group of Companies","Limited","Properties","Others"]
                },
                company    : {
                    type         : "text",
                    label        : "Vendor Company",
                    title        : "Enter Vendor company name"
                },
                name    : {
                    type         : "text",
                    label        : "Vendor Name",
                    title        : "Enter Vendor Name"
                },
                mobile    : {
                    type         : "text",
                    label        : "Vendor Mobile",
                    title        : "Enter Vendor mobile no"
                },
                binNo    : {
                    type         : "text",
                    label        : "Vendor BIN No",
                    title        : "Enter vendor BIN no"
                },
                address    : {
                    type         : "text",
                    label        : "Vendor Address",
                    title        : "Enter vendor address"
                }

            },
            filterDone      : function(data){
                if (data['vendorType'] === null) return "Please fill the vendor type";
                if (data['name'] === "") return "Please fill vendor name";
                if (data['mobile'] === "") return "Please fill vendor mobile no";
                return $.ajax({
                    url     : url,
                    type    : "post",
                    data    : data
                }).then(function(response){
                    if (response == false) return "Wrong username or password";
                });
            },
            top     : "auto"
        }).done(function(data){
            if(data === 'invalid'){
                $.MessageBox("This vendor already exist, Please try another company name");
            }else{
                location.reload();
            }
        });

    });

    $(document).on('change', '.vendor', function() {
        vendor = $(this).val();
        $.get(  "/"+lang+"/inventory/purchase/vendor-info", { vendor: vendor } )
            .done(function( response ) {
                obj = JSON.parse(response);
                $('#binNo').html(obj['binNo']);
                $('#vendorMobile').html(obj['vendorMobile']);
                $('#vendorAddress').html(obj['vendorAddress']);
            });


    });

    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#purchasePrice').val(obj['purchasePrice']);
                $('#salesPrice').val(obj['salesPrice']);
                $('#unit').html(obj['unit']);
            }
        })
    });


    $(document).on('click', '#addProduct', function() {

        var masterItem = $('#masterItem').val();
        var itemName = $('#itemName').val();
        var quantity = $('#quantity').val();
        var purchasePrice = $('#purchasePrice').val();
        var salesPrice = $('#salesPrice').val();
        var url = $('#addProduct').attr('data-action');
        if(masterItem === ''){
            $('#masterItem').select2('open');
            return false;
        }
        if(itemName === ''){
            $.MessageBox("Please enter purchase item name");
            $('#itemName').focus();
            return false;
        }
        if(purchasePrice === ''){
            $.MessageBox("Please enter purchase price");
            $('#purchasePrice').focus();
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter purchase quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'masterItem='+masterItem+'&itemName='+itemName+'&quantity='+quantity+'&purchasePrice='+purchasePrice,
            success: function (response) {
                obj = JSON.parse(response);
                $('#invoiceItems').html(obj['invoiceItems']);
                $('#subTotal').html(obj['subTotal']);
                $('#totalVat').html(obj['totalVat']);
                $('#total').html(obj['total']);
                $('#discount').html(obj['discount']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['netTotal']);
                $('#duable').html(obj['due']);
                if(obj['discountMode'] === "percent"){
                    $('#discountPercent').html(obj['discountPercent']+'%');
                }
                $('#purchasePrice').val('');
                $("#product").select2().select2("val","");
                $('#price').val('');
                $('#unit').html('Unit');
                $('#quantity').val('1');
            }
        })
    });

    $(document).on('change', '.quantity , .purchasePrice ,.salesPrice', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var purchasePrice = parseFloat($('#purchasePrice-'+id).val());
        var salesQuantity = parseFloat($('#salesQuantity-'+id).val());
        var salesPrice = parseFloat($('#salesPrice-'+id).val());
        var vatPercent = parseFloat($('#vatPercent-'+id).val());
        if(salesQuantity > quantity){
            $('#quantity-'+id).val($('purchaseQuantity-'+id).val());
            $.MessageBox("Purchase quantity must be more then sales quantity.");
            return false;
        }
        var subTotal  = (quantity * purchasePrice);
        var vat  = ((subTotal * vatPercent)/100);
        $("#subTotal-"+id).html(subTotal);
        $("#vatTotal-"+id).html(vat);
        $("#total-"+id).html(subTotal + vat);
        $.ajax({
            "url":  "/"+lang+"/inventory/purchase/purchase-item-update",
            type: 'POST',
            data:'purchaseItem='+ id +'&quantity='+ quantity +'&purchasePrice='+ purchasePrice+'&salesPrice='+ salesPrice,
            success: function(response) {
                obj = JSON.parse(response);
                $('#invoiceItems').html(obj['invoiceItems']);
                $('#subTotal').html(obj['subTotal']);
                $('#totalVat').html(obj['totalVat']);
                $('#total').html(obj['total']);
                $('#discount').html(obj['discount']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['netTotal']);
                $('#duable').html(obj['due']);
                if(obj['discountMode'] === "percent"){
                    $('#discountPercent').html(obj['discountPercent']+'%');
                }
            }

        })
    });

    $(document).on('click',".item-remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonDone  : "Yes",
            buttonFail  : "No",
            message     : "Are you sure you want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                obj = JSON.parse(response);
                $(event.target).closest('tr').hide();
                $('#subTotal').html(obj['subTotal']);
                $('#totalVat').html(obj['totalVat']);
                $('#total').html(obj['total']);
                $('#discount').html(obj['discount']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['netTotal']);
                $('#duable').html(obj['due']);
                if(obj['discountMode'] === "percent"){
                    $('#discountPercent').html(obj['discountPercent']+'%');
                }
            });
        });
    });


    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('form#postForm').on('change', '.payment', function (e) {
        var mrp = $(this).val();
        var netTotal = Number.parseFloat($("#paymentTotal").val());
        $('#payable').html(financial(mrp));
        due = financial(netTotal - mrp);
        $('#duable').html(due);
    });



    $(document).on('change', '.purchase', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                obj = JSON.parse(response);
                $('#subTotal').html(obj['subTotal']);
                $('#totalVat').html(obj['totalVat']);
                $('#total').html(obj['total']);
                $('#discount').html(obj['discount']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['paymentTotal']);
                $('#duable').html(obj['due']);
                if(obj['discountMode'] === "percent"){
                    $('#discountPercent').html(obj['discountPercent']+'%');
                }
            }
        });

    });

    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

    $(document).on('opened', '.remodal', function () {

        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });

});



