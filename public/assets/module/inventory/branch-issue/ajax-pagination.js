$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/inventory/branch-issue/data-table", // ajax source
            'data': function(data){

                var created = $('#created').val();
                var invoice = $('#invoice').val();
                var branch = $('#branch').val();
                var subTotal = $('#subTotal').val();
                var totalVat = $('#totalVat').val();
                var netTotal = $('#netTotal').val();
                var process = $('#process').val();


                // Append to data
                //  data._token = CSRF_TOKEN;

                data.created = created;
                data.invoice = invoice;
                data.branch = branch;
                data.subTotal = subTotal;
                data.totalVat = totalVat;
                data.netTotal = netTotal;
                data.process = process;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'created' },
            { "name": 'invoice','orderable':false  },
            { "name": 'branch' },
            { "name": 'subTotal','orderable':false  },
            { "name": 'totalVat','orderable':false  },
            { "name": 'netTotal' },
            { "name": 'process' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [6], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ]

    });

    $('#branch').keyup(function(){
        dataTable.draw();
    });
    $('#startDate').change(function(){
        dataTable.draw();
    });
    $('#endDate').change(function(){
        dataTable.draw();
    });
    $('#invoice').change(function(){
        dataTable.draw();
    });



});

