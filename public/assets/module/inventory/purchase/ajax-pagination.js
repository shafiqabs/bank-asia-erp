$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength":50, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/inventory/purchase/data-table", // ajax source
            'data': function(data){

                var created = $('#created').val();
                var received = $('#received').val();
                var invoice = $('#invoice').val();
                var companyName = $('#companyName').val();
                var method = $('#method').val();
                var subTotal = $('#subTotal').val();
                var discount = $('#discount').val();
                var calculation = $('#calculation').val();
                var tti = $('#tti').val();
                var total = $('#total').val();
                var payment = $('#payment').val();
                var due = $('#due').val();
                var mode = $('#mode').val();
                var process = $('#process').val();


                // Append to data
                //  data._token = CSRF_TOKEN;

                data.created = created;
                data.received = received;
                data.invoice = invoice;
                data.companyName = companyName;
                data.method = method;
                data.subTotal = subTotal;
                data.discount = discount;
                data.calculation = calculation;
                data.tti = tti;
                data.total = total;
                data.payment = payment;
                data.due = due;
                data.mode = mode;
                data.process = process;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'created' },
            { "name": 'received' },
            { "name": 'invoice' },
            { "name": 'companyName' },
            { "name": 'method' },
            { "name": 'subTotal','orderable':false},
            { "name": 'discount','orderable':false  },
            { "name": 'calculation','orderable':false},
            { "name": 'tti','orderable':false  },
            { "name": 'total','orderable':false  },
            { "name": 'payment','orderable':false  },
            { "name": 'due','orderable':false  },
            { "name": 'mode' },
            { "name": 'process' },
            { "name": 'action','orderable':false,"sClass":  "text-center"}

        ],
        "order": [
            [1, "asc"]
        ]

    });

    $('#vendor').keyup(function(){
        dataTable.draw();
    });
    $('#method').keyup(function(){
        dataTable.draw();
    });
    $('#startDate').change(function(){
        dataTable.draw();
    });
    $('#endDate').change(function(){
        dataTable.draw();
    });
    $('#received').change(function(){
        dataTable.draw();
    });
    $('#invoice').change(function(){
        dataTable.draw();
    });
    $('#discountType').change(function(){
        dataTable.draw();
    });
    $('#process').change(function(){
        dataTable.draw();
    });
    $('#mode').change(function(){
        dataTable.draw();
    });




});

