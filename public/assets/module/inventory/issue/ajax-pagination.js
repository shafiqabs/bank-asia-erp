$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/inventory/issue/data-table", // ajax source
            'data': function(data){

                var created = $('#created').val();
                var name = $('#name').val();
                var uom = $('#uom').val();
                var quantity = $('#quantity').val();
                var purchasePrice = $('#purchasePrice').val();
                var subTotal = $('#subTotal').val();
                var process = $('#process').val();


                // Append to data
                //  data._token = CSRF_TOKEN;

                data.created = created;
                data.name = name;
                data.uom = uom;
                data.quantity = quantity;
                data.purchasePrice = purchasePrice;
                data.subTotal = subTotal;
                data.process = process;
            }
        },
        'columns': [
            { "name": 'checkbox','orderable':false},
            { "name": 'id','orderable':false},
            { "name": 'created' },
            { "name": 'name' },
            { "name": 'uom','orderable':false },
            { "name": 'quantity','orderable':false },
            { "name": 'purchasePrice','orderable':false },
            { "name": 'subTotal','orderable':false },
            { "name": 'process' },
            { "name": 'action','orderable':false,width: "8%"}

        ],
        "aoColumnDefs" : [
            {"aTargets" : [8], "sClass":  "text-center"}
        ],
        "order": [
            [2, "desc"]
        ]

    });
    $('#startDate').change(function(){
        dataTable.draw();
    });
    $('#endDate').change(function(){
        dataTable.draw();
    });
    $('#process').change(function(){
        dataTable.draw();
    });
    $('#name').change(function(){
        dataTable.draw();
    });



});

