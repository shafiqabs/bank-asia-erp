$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

    //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/inventory/master-item/data-table", // ajax source
            'data': function(data){

                var hsCode = $('#hsCode').val();
                var groupName = $('#groupName').val();
                var categoryName = $('#categoryName').val();
                var name = $('#name').val();
                var unitName = $('#unitName').val();
                var vat = $('#vat').val();
                var cd = $('#cd').val();
                var sd = $('#sd').val();
                var rd = $('#rd').val();
                var ait = $('#ait').val();
                var at = $('#at').val();

                // Append to data
                //  data._token = CSRF_TOKEN;

                data.hsCode = hsCode;
                data.groupName = groupName;
                data.categoryName = categoryName;
                data.name = name;
                data.unitName = unitName;
                data.vat = vat;
                data.cd = cd;
                data.sd = sd;
                data.rd = rd;
                data.ait = ait;
                data.at = at;

            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'hsCode' },
            { "name": 'groupName' },
            { "name": 'categoryName' },
            { "name": 'name' },
            { "name": 'unitName','orderable':false  },
            { "name": 'vat','orderable':false  },
            { "name": 'cd' ,'orderable':false },
            { "name": 'sd','orderable':false  },
            { "name": 'rd','orderable':false  },
            { "name": 'ait','orderable':false  },
            { "name": 'at','orderable':false  },
            { "name": 'action','orderable':false  }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [6], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
            {
                "targets": 0,
                "orderable": false
            }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#category').keyup(function(){
        dataTable.draw();
    });
    $('#vat').keyup(function(){
        dataTable.draw();
    });
    $('#productType').change(function(){
        dataTable.draw();
    });
    $('#groupName').change(function(){
        dataTable.draw();
    });


});

