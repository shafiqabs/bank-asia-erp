$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}
pathUrl = window.location.href;
var pathname = window.location.pathname; // Returns path only (/path/example.html)
var segments = pathUrl.split( '/' );
var lang = segments[3];

$( document ).ready(function( $ ) {

     $(".select2-hscode-ajax").select2({

           placeholder: "Search HS code or name",
           ajax: {
               url: "/"+lang+"/inventory/master-item/product-hs-code", // ajax source
               dataType: 'json',
               delay: 250,
               data: function (params) {
                   return {
                       q: params.term // search term
                   };
               },
               results: function (data) {
                   return {
                       results: data
                   };
               },
               cache: true
           },

           formatResult: function (item) { return item.text}, // omitted for brevity, see the source of this page
           formatSelection: function (item) { return item.text }, // omitted for brevity, see the source of this page
           selectOnClose: true,
           allowClear: true,
           minimumInputLength:2
     });

    $(document).on('opened', '.remodal', function () {
        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });


    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

 /*   $('.hover').tooltip({
        title: fetchData,
        html: true,
        placement: 'right'
    });*/
});

function formCommonProcess() {

    $('.form-body').slimScroll({
        height: '400px'
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.mobileLocal').mask("00000-000000", {placeholder: "_____-______"});

    $('.checkboxToggle').bootstrapToggle();

    $('.multi-select2').multiSelect({ selectableOptgroup: true });

    $('#optgroup').multiSelect({ selectableOptgroup: true });
    $('.select2').select2({
        theme: 'bootstrap4'
    });
}

var explode = function AutoReload()
{
    $('#entityDatatable').each(function() {
        dt = $(this).dataTable();
        dt.fnDraw();
    });
    $('.form-submit').html("Save & Continue").prop("disabled", false);
}

function formSubmitProcess() {

    $("#postForm").validate({

        rules: {
            "customer_form[name]": {required: true},
            "customer_form[mobile]": {
                required: true,
                remote:window.location.pathname+"creatable/available"
            }

        },

        messages: {

            "customer_form[name]": "Enter user full name",
            "customer_form[mobile]":{
                required: "Please enter your mobile no.",
                remote: jQuery.validator.format("{0} username is already in use!")
            }
        },
        submitHandler: function(form) {
            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $('form#postForm')[0].reset();
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

function formEditSubmitProcess() {

    $("#postForm").validate({

        rules: {
            "customer_form[name]": {required: true},
            "customer_form[mobile]": {
                required: true,
                remote:window.location.pathname+"editable/available"
            }
        },

        messages: {
            "customer_form[name]": "Enter user full name",
            "customer_form[mobile]":{
                required: "Please enter your mobile no.",
                remote: jQuery.validator.format("{0} username is already in use!")
            }
        },
        submitHandler: function(form) {

            $(".form-submit").prop("disabled", true);
            $.ajax({
                url         : $('form#postForm').attr( 'action' ),
                type        : $('form#postForm').attr( 'method' ),
                data        : new FormData($('form#postForm')[0]),
                processData : false,
                contentType : false,
                beforeSend: function() {
                    $('.form-submit').html("Loading...").attr('disabled', 'disabled');
                },
                success: function(response){
                    $("#process-msg").show();
                    $(".alert-success").html(response);
                    setTimeout( explode, 2000);
                }
            });
        }
    });
}

